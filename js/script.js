const someArr = [1, 5, 'Abracadabra', null, undefined, 'Alohomora', 35, {}, true, 44, false];

// filterBy = (array, dataType) => array.filter(i => typeof i !== dataType);    // - без проверки на null

function filterBy (array, dataType) {
  if(dataType === null)
    return array.filter(i => i !== dataType);

  else if(dataType === 'object')
    return array.filter(i => typeof i !== dataType || i === null);

  else
    return array.filter(i => typeof i !== dataType);
};

testFilter = () => {
  console.log('Full array    --->   ', someArr);

  for (key of ['string', 'number', 'boolean', 'object', null, 'undefined']){
    console.log(`Without ${key}  ---> `, filterBy(someArr, key));
  }
}

testFilter();
